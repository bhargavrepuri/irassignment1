import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class CSE535Assignment {

	/**
 * @param args
 */
	

 class mem {
	 String docid;
	 Integer frequency;
}	
  class topk {
		String term;
		Integer freq;
 }
class docidcom implements Comparator<mem>
{
	public int compare(mem d1,mem d2){
		return d1.docid.compareTo(d2.docid);
	}
}
class freqcom implements Comparator<mem>
{
	public int compare(mem d1,mem d2){
		return d2.frequency.compareTo(d1.frequency);
	}
}
class termtopk implements Comparator<topk>
{
	public int compare(topk d1,topk d2){
		return d2.freq.compareTo(d1.freq);
	}
}
class termsortk implements Comparator<topk>
{
	public int compare(topk d1,topk d2){
		return d1.freq.compareTo(d2.freq);
	}
}
class docsort implements Comparator<String>
{
	public int compare(String d1,String d2){
		return d1.compareTo(d2);
	}
}
public void getTopk(LinkedList<topk> tk, int k1)
{
	Collections.sort(tk, new termtopk());
	System.out.print("Result: ");
	for(int r1=0;r1<k1-1;r1++)
	{
		System.out.print(tk.get(r1).term+", ");	
	}
	System.out.println(tk.get(k1-1).term);
}
public void termAtATimeQueryAnd(HashMap<String,LinkedList<mem>> fre,List<String> query_terms,int det,long startTime)
{

	int lu = query_terms.size();
	ArrayList<String> ans = new ArrayList<String>();
	String term1 = query_terms.get(0);
	
	//Add all the postings list of first term to answer list
	for(int t2=0;t2<fre.get(term1).size();t2++)
	{
		ans.add(fre.get(term1).get(t2).docid);
	}
	
	//for calculating comparisons
	int comp = 0;
	
	//Loop For each term and compare each docid of query postinglist with answer list
	for(int u1=1;u1<lu;u1++)
	{
		String term2 = query_terms.get(u1);
		ArrayList<String> tmp  = new ArrayList<String>();
	
	
		for(int l1=0;l1<ans.size();l1++)
		{
			for(int l2=0;l2<fre.get(term2).size();l2++)
			{
				comp++;
				//If docid of the query list is equal to docid of answer list then add it
				if(ans.get(l1).compareTo(fre.get(term2).get(l2).docid)==0)
				{
					tmp.add(ans.get(l1));
					break;
				}
			}
			
		}
		ans.clear();
		ans.addAll(tmp);
		tmp.clear();
	}
	long stopTime = System.currentTimeMillis();
	long elapsedtime = (stopTime-startTime)/1000;
	if(det==0)
	{
		
		System.out.println(ans.size()+" documents are found");
		System.out.println(comp+" comparisons are made");
		System.out.println(elapsedtime+" seconds are used");
	}	
	else
	{
		//This module is for Optimization
			System.out.println(comp+" comparisons are made with optimization");
			if(ans.size()==0)
				System.out.print("Result: 0 documents found");
			else
			{
				Collections.sort(ans, new docsort());
				System.out.print("Result: ");
				for(int h3=0;h3<ans.size()-1;h3++)
				{
					System.out.print(ans.get(h3)+ ", ");
				}
				System.out.print(ans.get(ans.size()-1));
			}
			System.out.println();
			}
}


public void termAtATimeQueryOr(HashMap<String,LinkedList<mem>> fre,List<String> query_terms,int det,long startTime)
{
	
		int lu = query_terms.size();
		ArrayList<String> ans = new ArrayList<String>();
		String term1 = query_terms.get(0);
		
		for(int t2=0;t2<fre.get(term1).size();t2++)
		{
			ans.add(fre.get(term1).get(t2).docid);
		}
		
		//For Calculating Comparisons
		int comp = 0;
	    
		//Loop For each term and compare each docid of query postinglist with answer list
		for(int u1=1;u1<lu;u1++)
		{
		
			String term2 = query_terms.get(u1);
			ArrayList<String> tmp  = new ArrayList<String>();
			for(int l1=0;l1<fre.get(term2).size();l1++)
			{
				 int flag = 0;
				for(int l2=0;l2<ans.size();l2++)
				{
					comp++;
					//If docid of the query list is equal to docid of answer list then do not add it
					if(ans.get(l2).compareTo(fre.get(term2).get(l1).docid)==0)
					{
						flag = 1;
						break;
					}
				}
				if(flag ==0)
				{
					tmp.add(fre.get(term2).get(l1).docid);
				}
			}
			//ans.clear();
			ans.addAll(tmp);
			tmp.clear();
		}
		long stopTime = System.currentTimeMillis();
		long elapsedtime = (stopTime-startTime)/1000;
	if(det==0)
	{
	
		System.out.println(ans.size()+" documents are found");
		System.out.println(comp+" comparisons are made");
		System.out.println(elapsedtime+" seconds are used");
	}	
	else
	{
		//This module is for Optimization
			System.out.println(comp+" comparisons are made with optimization");
			if(ans.size()==0)
				System.out.print("Result: 0 documents found");
			else
			{
				Collections.sort(ans, new docsort());
				System.out.print("Result: ");
				for(int h3=0;h3<ans.size()-1;h3++)
				{
					System.out.print(ans.get(h3)+ ", ");
				}
				System.out.print(ans.get(ans.size()-1));
			}
			System.out.println();
			}
}

public void DocAtATimeQueryAnd(HashMap<String,LinkedList<mem>> doc,List<String> query_terms)
{

	int lu = query_terms.size();
	ArrayList<String> ans = new ArrayList<String>();
	ArrayList<String> tmp = new ArrayList<String>();
		
	//For Calculating comparisons
	int comp = 0;
	ArrayList<Integer> point = new ArrayList(query_terms.size());
	//Initiate queries posting lists to zeroth index
	for(int w1=0;w1<lu;w1++)
	{
		point.add(0);
		
	}
	//Loop For each doc
	
	int flag_terms = 0;
	while(flag_terms!=1)
	{
		//find max doc id
		int max_pos = Integer.MIN_VALUE;
		for(int u1=0;u1<query_terms.size();u1++)
		{
		
			comp++;
			
				if(Integer.valueOf(doc.get(query_terms.get(u1)).get(point.get(u1)).docid)>max_pos)
				{
					max_pos = Integer.valueOf(doc.get(query_terms.get(u1)).get(point.get(u1)).docid);
				}
			
		}
		int flag1 = 1;
		for(int u2=0;u2<query_terms.size();u2++)
		{
			comp++;
			//To check if each term posting list has reached its end
			if(point.get(u2)>=doc.get(query_terms.get(u2)).size()-1)
			{
				flag_terms = 1;
			}
		    //to check if the term docid is less than the maximum docid
			if(Integer.valueOf(doc.get(query_terms.get(u2)).get(point.get(u2)).docid)<max_pos)
			{	
				
				flag1 = 0;
				point.set(u2, point.get(u2)+1);
				
			}
			
		}
		//if all the docid's of different terms is equal to maximum docid then add id
		if(flag1==1)
		{
			ans.add(doc.get(query_terms.get(0)).get(point.get(0)).docid);
			for(int k2=0;k2<query_terms.size();k2++)
			{	
					point.set(k2, point.get(k2)+1);
			}	
		}
		
	}
	
	
	System.out.println(ans.size()+" documents are found");
	System.out.println(comp+" comparisons are made");
	if(ans.size()==0)
		System.out.print("Result: 0 documents found");
	else
	{
		System.out.print("Result: ");
		for(int h3=0;h3<ans.size()-1;h3++)
		{
			System.out.print(ans.get(h3)+ ", ");
		}
		System.out.print(ans.get(ans.size()-1));
	}
	System.out.println();
}

public void DocAtATimeQueryOr(HashMap<String,LinkedList<mem>> doc,List<String> query_terms)
{
	
	int lu = query_terms.size();
	ArrayList<String> ans = new ArrayList<String>();
		
	//For Calculating comparisons
	int comp = 0;
	ArrayList<Integer> point = new ArrayList(query_terms.size());
	
	//Initiate queries posting lists to zeroth index
	for(int w1=0;w1<lu;w1++)
	{
		point.add(0);
	}
	
	//Loop For each doc
	int flag_terms = 0;
	while(flag_terms<query_terms.size())
	{
		//find max doc id
		flag_terms = 0;
		int min_pos = Integer.MAX_VALUE;
		//To calculate the minimum docid
		for(int u1=0;u1<query_terms.size();u1++)
		{
			if(point.get(u1)<=doc.get(query_terms.get(u1)).size()-1)
			{
				comp++;
			
				if(Integer.valueOf(doc.get(query_terms.get(u1)).get(point.get(u1)).docid)<min_pos)
				{
					min_pos = Integer.valueOf(doc.get(query_terms.get(u1)).get(point.get(u1)).docid);
				}
			}
		}
		int flag1 = 1;
		//For each term we will the document pointers
		for(int u2=0;u2<query_terms.size();u2++)
		{
			//To check if each term posting list has reached its end	
			if(point.get(u2)>doc.get(query_terms.get(u2)).size()-1)
			{
				flag_terms++;				
			}
			else
			{
				comp++;
				//To check if query term docid is equal to minimum element then add it if it is not in answer list
				if(Integer.valueOf(doc.get(query_terms.get(u2)).get(point.get(u2)).docid)==min_pos&&flag1==1)
				{
					
					ans.add(doc.get(query_terms.get(u2)).get(point.get(u2)).docid);
					point.set(u2, point.get(u2)+1);
					flag1 = 0;
				}
				//To check if query term docid is equal to minimum element and present in answer list then just increase the pointer of the term 
				//Note that we do not increase the pointer only if its docid>min_pos 
				else if(Integer.valueOf(doc.get(query_terms.get(u2)).get(point.get(u2)).docid)==min_pos&&flag1==0)
				{
					point.set(u2, point.get(u2)+1);
				}
			}
		}
		
	}

	
	System.out.println(ans.size()+" documents are found");
	System.out.println(comp+" comparisons are made");
	if(ans.size()==0)
		System.out.print("Result: 0 documents found");
	else
	{
		System.out.print("Result: ");
		for(int h3=0;h3<ans.size()-1;h3++)
		{
			System.out.print(ans.get(h3)+ ", ");
		}
		System.out.print(ans.get(ans.size()-1));
	}
	System.out.println();
}


public void getPostings(HashMap<String,LinkedList<mem>> doc, HashMap<String,LinkedList<mem>> fre, String q_term)
{
		//Ordered by docId
		System.out.println("FUNCTION:getPostings "+q_term);
	    LinkedList<mem> q_t = new LinkedList<mem>();
	    q_t = doc.get(q_term);
	    if(q_t==null)
	    {
	    	System.out.println("term not found");
	    }
	    else
	    {
	    	System.out.print("Ordered by doc IDs:");
	    	for(int q1 = 0;q1<q_t.size()-1;q1++)
	    	{
	    	System.out.print(q_t.get(q1).docid+", ");	
	    	}
	    	System.out.println(q_t.get(q_t.size()-1).docid);
	    
	    	//Ordered by term frequency
	    	System.out.println("FUNCTION:getPostings "+q_term);
			System.out.print("Ordered by TF:");
	    	LinkedList<mem> f_t = new LinkedList<mem>();
	    	f_t = fre.get(q_term);
	    	for(int q2 = 0;q2<f_t.size()-1;q2++)
	    	{
	    	System.out.print(f_t.get(q2).docid+", ");	
	    	}
	   
	    	System.out.println(f_t.get(f_t.size()-1).docid);
	    }
}
public static void main(String[] args) {
	// TODO Auto-generated method stub
	CSE535Assignment TL = new CSE535Assignment();
	
	String filename = args[0];
	String filename_input = args[3];
	List<String> zoom = new ArrayList<>();
	try{
		FileReader inputFile = new FileReader(filename_input);
		
		BufferedReader bufferReader_input = new BufferedReader(inputFile);
		
		String input_line;
		
		while((input_line = bufferReader_input.readLine()) != null)
		{
			zoom.add(input_line);
		}
		bufferReader_input.close();
	}
	catch (Exception e1){
		System.out.println("Input file is not available");
	}
	
	try{

          //Create object of FileReader
          FileReader dataFile = new FileReader(filename);

          //Instantiate the BufferedReader Class
          BufferedReader bufferReader = new BufferedReader(dataFile);

          //Variable to hold the one line data
          String line;
          //doc Hashmap to index data with ordering strategy of increasing doc ID's
          HashMap<String,LinkedList<mem>> doc = new HashMap();
        //doc Hashmap to index data with ordering strategy of increasing doc ID's
          HashMap<String,LinkedList<mem>> fre = new HashMap();
          LinkedList<topk> tk = new LinkedList();
          LinkedList<topk> tk1 = new LinkedList();
          
          int count=0;
          // Read file line by line and parse the input file
          while ((line = bufferReader.readLine()) != null)   {
              StringBuilder term = new StringBuilder(line);
              LinkedList<mem> val = new LinkedList();
              LinkedList<mem> val_n = new LinkedList();
              topk pk = TL.new topk();
              
              for(int i=0;i<term.length();i++)
              {
            	  while(term.charAt(i)!='\\')
            	  {
            		  i++;
            	  }
            	  StringBuilder s = new StringBuilder();
        		  s.append(term.substring(0,i));
        		  String key_s = s.toString();
        		  //key_s contains term
        		  pk.term = key_s.trim();
            	  i = i+2;
            	  int j = i;
            	  while(term.charAt(i)!='\\')
            	  {
            		  i++;
            	  }
            	  StringBuilder s1 = new StringBuilder();
            	  s1.append(term.substring(j,i));
            	  String s2 = s1.toString();
            	  //s2 contains the size of the posting list of the respective term  
            	  int p = Integer.valueOf(s2.trim());
            	  pk.freq = p;
            	  tk.add(pk);
            	 
            	  i = i+2;
            	  int k = i+1;
            	  while(term.charAt(i)!=']')
            	  {
            		  i++;
            	  }
            	  StringBuilder s3 = new StringBuilder();
            	  s3.append(term.substring(k, i));
            	  String s4 = s3.toString();
            	 //posting list contains docid/frequency terms
            	  List<String> postinglist = Arrays.asList(s4.split(","));
            	 
            	  for(int e =0;e<postinglist.size();e++)
            	  {
            		  mem t = TL.new mem();
            		  List<String> pl = Arrays.asList(postinglist.get(e).toString().split("/")); 
            		  t.docid = pl.get(0).trim();
            		  t.frequency = Integer.valueOf(pl.get(1).trim());
            		  val.add(t);
            		  val_n.add(t);
            		
            	  }
            	  //sorting based on docid to insert into doc hashmap
            	  Collections.sort(val, TL.new docidcom());
    
            	  doc.put(key_s, val);
            	  //sorting based on term frequency to insert into fre hashmap
            	  Collections.sort(val_n, TL.new freqcom());
            	  fre.put(key_s, val_n);   	            	  
              }
        	  
          }
         //To print output to log file
          PrintStream out = new PrintStream(new FileOutputStream(args[1]));
          System.setOut(out);
          
          //getTopK Function
          System.out.println("FUNCTION: getTopK "+Integer.valueOf(args[2])); 
          TL.getTopk(tk,Integer.valueOf(args[2]));
          
          // Loop for each line of input to get getpostings list and calling TAAT and DAAT funcions
          for(int z1 = 0;z1<zoom.size();z1++)
          {
        	  LinkedList<topk> srtterm = new LinkedList<topk>();
        	  List<String> query_terms = Arrays.asList(zoom.get(z1).split(" "));
        	  ArrayList<String> query_terms_sort = new ArrayList<String>() ;
        	  ArrayList<String> query_terms_mod = new ArrayList<String>() ;
        	  
        	  for(int z2 = 0;z2<query_terms.size();z2++)
        	  {
        		  if(doc.get(query_terms.get(z2))!=null)
        		  {
        			  query_terms_mod.add(query_terms.get(z2));
        		  }
        		  TL.getPostings(doc, fre,query_terms.get(z2));
        	  }
        	  //and_flag to see if any of the query term is not in the index
        	  int and_flag = 0,or_count=0;
        	  for(int z3 = 0;z3<query_terms.size();z3++)
        	  {
        		  topk pk1 = TL.new topk();
        		  pk1.term = query_terms.get(z3);
        		  if(doc.get(pk1.term)!=null)
        		  {
        			  pk1.freq = doc.get(pk1.term).size();
        		      srtterm.add(pk1);
        		  }
        		  else
        		  {
        			  or_count++;
        			  and_flag = 1;
        		  }
        	  }
        	  
        	  // sorting query_terms to obtain optimized solution for TAAT
        	  Collections.sort(srtterm, TL.new termsortk());
        	  
        	  for(int z4 = 0;z4<srtterm.size();z4++)
        	  {
        			  query_terms_sort.add(srtterm.get(z4).term);
        	  }
        	 
        	  long startTimeAnd = System.currentTimeMillis();
        	  
        	  //Calling TermAtATimeQueryAnd function
        	  System.out.print("FUNCTION:TermAtATimeQueryAnd ");
    		  for(int yu = 0;yu<query_terms.size()-1;yu++)
    		  {
    			  System.out.print(query_terms.get(yu)+", ");
    		  }
    		  System.out.println(query_terms.get(query_terms.size()-1));
        	  if(and_flag ==0)
        	  {
        		  TL.termAtATimeQueryAnd(fre,query_terms,0,startTimeAnd);
        	  }
        	  else
        	  {
        		  System.out.println("terms not found");
        	  }
        	  //Optimization for TermAtATimeQueryAnd
        	  
        	  if(and_flag ==0)
        	  {
        		  long startTimeAndOpt = System.currentTimeMillis();
        		  TL.termAtATimeQueryAnd(fre,query_terms_sort,1,startTimeAndOpt);
        	  }
        	  
        	//Calling TermAtATimeQueryAnd function and Optimization for TermAtATimeQueryAnd
        	  System.out.print("FUNCTION:TermAtATimeQueryOr ");
    		  for(int yu = 0;yu<query_terms.size()-1;yu++)
    		  {
    			  System.out.print(query_terms.get(yu)+", ");
    		  }
    		  System.out.println(query_terms.get(query_terms.size()-1));
        	  if(and_flag==0)
        	  {
        		  long startTimeOr = System.currentTimeMillis();
        		  TL.termAtATimeQueryOr(fre,query_terms,0,startTimeOr);
        		  long startTimeOrOpt = System.currentTimeMillis();
        		  TL.termAtATimeQueryOr(fre,query_terms_sort,1,startTimeOrOpt);
        	  }
        	  else if(or_count!=query_terms.size())
        	  {
        		  long startTimeOr = System.currentTimeMillis();
        		  TL.termAtATimeQueryOr(fre,query_terms_mod,0,startTimeOr);
        		  long startTimeOrOpt = System.currentTimeMillis();
        		  TL.termAtATimeQueryOr(fre,query_terms_sort,1,startTimeOrOpt);
        	  }
        	  else if(or_count==query_terms.size())
        	  {
        		  System.out.println("terms not found");
        	  }
        	  //Calling DocAtATimeAnd function
        	  System.out.print("FUNCTION:DocAtATimeQueryAnd ");
    		  for(int yu = 0;yu<query_terms.size()-1;yu++)
    		  {
    			  System.out.print(query_terms.get(yu)+", ");
    		  }
    		  System.out.println(query_terms.get(query_terms.size()-1));
    		  
        	  if(and_flag==0)
        	  {
        		  TL.DocAtATimeQueryAnd(doc,query_terms);
        	  }
        	  else
        	  {
        		  System.out.println("terms not found");
        	  }
        	  
        	//Calling DocAtATimeOR function
        	  System.out.print("FUNCTION:DocAtATimeQueryOr ");
    		  for(int yu = 0;yu<query_terms.size()-1;yu++)
    		  {
    			  System.out.print(query_terms.get(yu)+", ");
    		  }
    		  System.out.println(query_terms.get(query_terms.size()-1));
        		if(and_flag==0) 
        		{
        			TL.DocAtATimeQueryOr(doc,query_terms);
        		}
        		else
        		{
        			if(or_count!=query_terms.size())
        			{
        				TL.DocAtATimeQueryOr(doc,query_terms_mod);
        			}
        			else
        			{
        				System.out.println("terms not found");
        			}
        		}
          }
  		 
         
          bufferReader.close();
       }catch(Exception e){

    	   System.out.println("Error while reading file line by line:" + e.getMessage());                      
	       }

	}

}
